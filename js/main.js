var estado = 0;

const cigarro = document.getElementById("cigarro");

const btn1Peso = document.getElementById("btn1Peso");
const btn2Pesos = document.getElementById("btn2Pesos");
const btn5Pesos = document.getElementById("btn5Pesos");
const btnCambio = document.getElementById("btnCambio");
const btnCancelar = document.getElementById("btnCancelar");
const btnCigarro = document.getElementById("btnCigarro");

const lblEstado = document.getElementById("lblEstado");
const lblCambio = document.getElementById("lblCambio");

const estado1Peso = {
  0: 1,
  1: 2,
  2: 3,
  3: 4,
  4: 5,
};

const estado2Pesos = {
  0: 2,
  1: 3,
  2: 4,
  3: 5,
  4: 6,
};

const estado5Pesos = {
  0: 5,
  1: 6,
  2: 7,
  3: 8,
  4: 9,
};

const desabilitarBotones = () => {
  btn1Peso.disabled = true;
  btn2Pesos.disabled = true;
  btn5Pesos.disabled = true;
  btnCancelar.disabled = true;
  btn1Peso.style.textDecoration = `line-through`;
  btn2Pesos.style.textDecoration = `line-through`;
  btn5Pesos.style.textDecoration = `line-through`;
  btnCancelar.style.textDecoration = `line-through`;
};

const habilitarBotones = () => {
  btn1Peso.disabled = false;
  btn2Pesos.disabled = false;
  btn5Pesos.disabled = false;
  btnCancelar.disabled = false;
  btn1Peso.style.textDecoration = `none`;
  btn2Pesos.style.textDecoration = `none`;
  btn5Pesos.style.textDecoration = `none`;
  btnCancelar.style.textDecoration = `none`;
};

const esFinal = (e) => {
  if (estado >= 5 && estado <= 9) {
    e.preventDefault();

    const audio = new Audio("/media/cigarro.mp3");

    audio.addEventListener("ended", () => {
      cigarro.style.visibility = `visible`;
      desabilitarBotones();
      lblCambio.innerHTML = estado - 5;
    });

    audio.play();
  } else {
    habilitarBotones();
  }
};

const uno = (e) => {
  desabilitarBotones();
  e.preventDefault();

  const audio = new Audio("/media/moneda.mp3");

  audio.addEventListener("ended", () => {
    estado = estado1Peso[estado];
    lblEstado.innerHTML = estado;
    esFinal(e);
  });

  audio.play();
};

const dos = (e) => {
  desabilitarBotones();
  e.preventDefault();

  const audio = new Audio("/media/moneda.mp3");

  audio.addEventListener("ended", () => {
    estado = estado2Pesos[estado];
    lblEstado.innerHTML = estado;
    esFinal(e);
  });

  audio.play();
};

const cinco = (e) => {
  desabilitarBotones();
  e.preventDefault();

  const audio = new Audio("/media/moneda.mp3");

  audio.addEventListener("ended", () => {
    estado = estado5Pesos[estado];
    lblEstado.innerHTML = estado;
    esFinal(e);
  });

  audio.play();
};

const recojerCambio = (e) => {
  e.preventDefault();

  const audio = new Audio("/media/cambio.mp3");

  audio.addEventListener("ended", () => {
    lblCambio.innerHTML = 0;
    btnCambio.disabled = false;
  });

  if (lblCambio.innerHTML > 0) {
    btnCambio.disabled = true;
    audio.play();
  }
};

const cancelar = (e) => {
  if (estado > 0 && estado <= 4) {
    lblCambio.innerHTML = estado;
    estado = 0;
    lblEstado.innerHTML = estado;
    recojerCambio(e);
  }
};

const recojerCigarro = () => {
  if (cigarro.style.visibility == `visible`) {
    estado = 0;
    lblEstado.innerHTML = estado;
    cigarro.style.visibility = `hidden`;
    habilitarBotones();
  }
};

btn1Peso.addEventListener("click", uno);
btn2Pesos.addEventListener("click", dos);
btn5Pesos.addEventListener("click", cinco);
btnCambio.addEventListener("click", recojerCambio);
btnCancelar.addEventListener("click", cancelar);
btnCigarro.addEventListener("click", recojerCigarro);
